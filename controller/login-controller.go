package controller

import (
	"gobio2/global"
	"gobio2/service"

	"gobio2/dto"

	"fmt"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type LoginController interface {
	Login(ctx *gin.Context) string
}

type registerController interface {
	Register(ctx *gin.Context) string
}

type loginController struct {
	loginService service.LoginService
	jWtService   service.JWTService
}

func NewLoginController(loginService service.LoginService,
	jWtService service.JWTService) LoginController {
	return &loginController{
		loginService: loginService,
		jWtService:   jWtService,
	}
}

func (controller *loginController) Login(ctx *gin.Context) string {
	var credentials dto.Credentials
	fmt.Println("LoginController")
	err := ctx.ShouldBind(&credentials)
	if err != nil {
		return ""
	}
	isAuthenticated := controller.loginService.Login(credentials.Username, credentials.Password)
	if isAuthenticated {
		return controller.jWtService.GenerateToken(credentials.Username, true)
	}
	return ""
}

func FindExistingEmail(username string) bool {
	db := global.DB
	find := false
	rows, err := db.Query("select email,password from users where email = ?;", username)
	if err != nil {
		fmt.Print("error db", err.Error())
	}
	defer rows.Close()
	var dbcredentials dto.Credentials
	for rows.Next() {
		fmt.Println("db row")
		find = true
		err = rows.Scan(&dbcredentials.Username, &dbcredentials.Password)
	}

	// if find == true {

	// 	err := bcrypt.CompareHashAndPassword([]byte(dbcredentials.Password), []byte(password))

	// 	fmt.Println("error hash = ", err)
	// 	if err != nil {
	// 		log.Println(err)
	// 		return false
	// 	}
	// 	log.Println(err)
	// }

	// fmt.Print("end db --->", dbcredentials.Password)
	rows.Close()
	return find
}

func PostRegister(ctx *gin.Context) string {
	var credentials dto.RegistrationCredentials
	fmt.Println("Registration Controller")
	err := ctx.ShouldBind(&credentials)
	if err != nil {
		return ""
	}
	if AllowedEmail(credentials.Email) {

	} else {
		fmt.Println("Unknown user : not registered")
		// return "Utilisateur non inscrit : " + credentials.Email
		return credentials.Email
	}

	fmt.Println("Postregistration performed, ok")

	if FindExistingEmail(credentials.Email) {
		fmt.Println("user already exists")
		return ""
	} else {
		fmt.Println("user can be created")
	}

	if credentials.Password != credentials.Password2 {
		fmt.Println("Password are not the same")
		return ""
	} else {
		fmt.Println("passwords are ok")
	}
	// isAuthenticated := controller.loginService.Login(credentials.Username, credentials.Password)
	// if isAuthenticated {
	// 	return controller.jWtService.GenerateToken(credentials.Username, true)
	// }

	hash, _ := bcrypt.GenerateFromPassword([]byte(credentials.Password), bcrypt.MinCost)
	fmt.Println("register hash = ", hash)
	db := global.DB
	// insForm, err := db.Prepare("INSERT INTO users(firstname, lastname, email, password, selas) VALUES(?,?,?,?,?)")
	insForm, err := db.Prepare("INSERT INTO users(email, password) VALUES(?,?)")
	if err != nil {
		panic(err.Error())
	}
	// insForm.Exec(credentials.Firstname, credentials.Lastname, credentials.Email, hash, credentials.Selas)
	insForm.Exec(credentials.Email, hash)
	defer insForm.Close()

	// isAuthenticated := loginController.loginService.Login(credentials.Email, credentials.Password)
	// if isAuthenticated {
	// 	return loginController.jWtService.GenerateToken(credentials.Email, true)
	// }
	// return "ko"
	return ""
	// c.HTML(http.StatusOK, "home.tmpl", nil)
}

func AllowedEmail(email string) bool {
	db := global.DB
	find := false
	rows, err := db.Query("select email from users where email = ?;", email)
	if err != nil {
		fmt.Print("error db", err.Error())
	}
	defer rows.Close()
	for rows.Next() {
		find = true
		break
	}

	rows.Close()
	return find
}
