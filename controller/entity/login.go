package entity

type Login struct {
	Username string `json:"username"`
	Password string `json:"password" bingind="required"`
}

type Registration struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Username  string `json:"username"`
	Password  string `json:"password" bingind="required"`
	Password2 string `json:"password2" bingind="required"`
}
