package handler

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

type LoginForm struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func Login(c *gin.Context) {
	c.HTML(http.StatusOK, "login.tmpl", nil)
}

func Flex(c *gin.Context) {
	c.HTML(http.StatusOK, "flex.tmpl", nil)
}

func Profil(c *gin.Context) {
	c.HTML(http.StatusOK, "profil.tmpl", nil)
}

func Registration(c *gin.Context) {
	c.HTML(http.StatusOK, "registration.tmpl", nil)
}

func Password(c *gin.Context) {
	c.HTML(http.StatusOK, "password.tmpl", nil)
}

func PasswordMailSent(c *gin.Context) {
	c.HTML(http.StatusOK, "passwordmailsent.tmpl", nil)
}

func PasswordError(c *gin.Context) {
	c.HTML(http.StatusOK, "passworderror.tmpl", nil)
}

func Logout(ctx *gin.Context) {

	s_env_domain := os.Getenv("DOMAIN")
	// ctx.SetCookie("token", "", -1, "/", "localhost", false, true)
	ctx.SetCookie("token", "", -1, "/", s_env_domain, true, true)
	// fmt.Println("redirection home ")
	ctx.Redirect(http.StatusFound, "/")

}

func NoRoute(ctx *gin.Context) {

	ctx.HTML(http.StatusOK, "noroute.tmpl", nil)

}

// Post
func PostLogin(c *gin.Context) {
	var form LoginForm
	c.Bind(&form)
	emailValue := form.Email
	// emailValue := c.PostForm("email")
	// c.HTML(http.StatusOK, "login.tmpl", nil)
	fmt.Println("form = ", form)
	// fmt.Println(emailValue)
	// fmt.Println(emailValue1)
	c.JSON(http.StatusOK, gin.H{
		// "message": fmt.Sprintf(" %s successfully created", form.Email),
		"message": "login",
		"email":   emailValue,
	})
}

func CheckToken(c *gin.Context) {
	fmt.Println("check Token")
	// cookie, err := c.Cookie("token")
	// if err != nil {
	// 	fmt.Println("error : token not set", cookie)
	// }
	// func (jwtSrv *jwtService) ValidateToken(tokenString string) (*jwt.Token, error) {
	// return jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
	// 	// Signing method validation
	// 	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
	// 		return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
	// 	}
	// 	// Return the secret signing key
	// 	return []byte(jwtSrv.secretKey), nil
	// })
	// c.JSON(http.StatusOK, gin.H{
	// 	// "message": fmt.Sprintf(" %s successfully created", form.Email),
	// 	"checkToken": cookie,
	// })
	c.HTML(http.StatusOK, "cookie.tmpl", nil)
}
