package handler

import (
	"fmt"
	"gobio2/global"
	"golang.org/x/crypto/bcrypt"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"

	"net/http"
	"net/smtp"

	"github.com/gin-gonic/gin"
)

type PassForm struct {
	Email string `form:"email" json:"email"`
}

func SendEmailPassword(ctx *gin.Context) string {
	var form PassForm
	ctx.Bind(&form)
	emailToValue := form.Email
	if ExistEmail(emailToValue) {

		fmt.Println("email to = ", emailToValue)
		s_email_from := os.Getenv("EMAIL_FROM")
		s_email_pwd := os.Getenv("EMAIL_PWD")
		// s_email_site := os.Getenv("EMAIL_SITE")
		s_email_host := os.Getenv("EMAIL_HOST")

		// Set up authentication information.
		// auth := smtp.PlainAuth("", s_email_from, s_email_pwd, s_email_site)
		// auth := smtp.PlainAuth("", s_email_from, s_email_pwd, "khyoz.work")
		auth := smtp.PlainAuth("", s_email_from, s_email_pwd, s_email_host)
		fmt.Println("auth = ", auth)
		// Connect to the server, authenticate, set the sender and recipient,
		// and send the email all in one step.
		to := []string{"khyozsan@gmail.com"}

		newPassword := RandomPassword()
		SaveNewPassword(emailToValue, newPassword)

		msg := []byte(
			"Votre nouveau mot de passe est :   " + newPassword + " \r\n" +
				"\r\n" +
				"Pensez à le changer si possible en passant par le menu profil.\r\n")
		// err := smtp.SendMail("khyoz.work:25", auth, s_email_from, to, msg)
		err := smtp.SendMail(s_email_host+":25", auth, s_email_from, to, msg)
		if err != nil {
			log.Fatal(err)
		}
		ctx.Redirect(http.StatusFound, "/passwordmailsent")
		return ""
	} else {
		ctx.Redirect(http.StatusFound, "/passworderror")
		return ""
	}

}

func SaveNewPassword(email string, password string) {
	hash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	fmt.Println("register hash = ", hash)
	db := global.DB
	// insForm, err := db.Prepare("INSERT INTO users(firstname, lastname, email, password, selas) VALUES(?,?,?,?,?)")
	insForm, err := db.Prepare("UPDATE users set comment = ?, password = ? where email = ?;")
	if err != nil {
		panic(err.Error())
	}
	// insForm.Exec(credentials.Firstname, credentials.Lastname, credentials.Email, hash, credentials.Selas)
	insForm.Exec(password, hash, email)
	fmt.Println("db password changed to ", password, " for user ", email)
	defer insForm.Close()
}

func RandomPassword() string {
	rand.Seed(time.Now().UnixNano())
	// chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ" +
	// 	"abcdefghijklmnopqrstuvwxyzåäö" +
	// 	"0123456789")
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXY&$:" +
		"abcdefghijklmnopqrstuvwxyz²!_" +
		"0123456789")
	length := 8
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	str := b.String() // E.g. "ExcbsVQs"
	fmt.Println(str)
	return str
}

func ExistEmail(email string) bool {
	db := global.DB
	find := false
	rows, err := db.Query("select email from users where email = ?;", email)
	if err != nil {
		fmt.Print("error db", err.Error())
	}
	defer rows.Close()
	for rows.Next() {
		find = true
		break
	}

	rows.Close()
	return find
}
