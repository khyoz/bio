package main

// https://www.youtube.com/watch?v=NoDRq6Twkts
// https://golang.org/src/encoding/base64/example_test.go

import (
	"fmt"
	"gobio2/controller"
	"gobio2/global"
	"gobio2/handler"
	"gobio2/middlewares"
	"gobio2/service"
	"net/http"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

var (
	// videoService service.VideoService = service.New()
	loginService service.LoginService = service.NewLoginService()
	jwtService   service.JWTService   = service.NewJWTService()

	// videoController controller.VideoController = controller.New(videoService)
	loginController controller.LoginController = controller.NewLoginController(loginService, jwtService)
)

func main() {
	global.InitDB()

	server := gin.Default()
	server.Use(cors.Default())
	server.LoadHTMLGlob("templates/*.tmpl")
	// server.LoadHTMLGlob("templates/partial/*.tmpl")
	server.Static("/assets", "./public")

	// server.GET("/home", controller.Homepage)
	// authRoutes := server.Group("/", middlewares.AuthorizeJWT())
	// {
	// 	authRoutes.GET("/home", controller.Homepage)
	// }
	server.GET("/", handler.Login)
	authRoutes := server.Group("/", middlewares.AuthorizeCookie())
	{
		authRoutes.GET("/home", controller.Homepage)
		authRoutes.GET("/profil", handler.Profil)
		// authRoutes.GET("/logout", loginController.Logout(ctx))
	}

	server.GET("/debug", controller.Homepage)
	server.GET("/login", handler.Login)
	server.GET("/registration", handler.Registration)
	server.GET("/password", handler.Password)
	server.GET("/passwordmailsent", handler.PasswordMailSent)
	server.GET("/logout", handler.Logout)
	server.GET("/passworderror", handler.PasswordError)
	// server.GET("/pine", controller.Homepage)

	server.POST("/resetpassword", func(ctx *gin.Context) {
		token := handler.SendEmailPassword(ctx)
		fmt.Println("mail sent ", token)
	})

	server.POST("/register", func(ctx *gin.Context) {
		token := controller.PostRegister(ctx)
		fmt.Println("registration token = ", token)
		// ctx.Redirect(http.StatusFound, "/test")
		if token != "" {
			// ctx.Redirect(http.StatusFound, "/login")
			data := gin.H{
				"email": token,
			}
			ctx.HTML(http.StatusOK, "unregistered.tmpl", data)
		} else {
			ctx.Redirect(http.StatusFound, "/login")
		}

	})

	server.POST("/postlogin", func(ctx *gin.Context) {
		token := loginController.Login(ctx)
		if token != "" {
			fmt.Println("token = ", token)

			s_env_domain := os.Getenv("DOMAIN")
			// ctx.SetCookie("token", token, 3600, "/", "localhost", false, true)
			ctx.SetCookie("token", token, 36000, "/", s_env_domain, true, true)
			fmt.Println("redirection home ")
			ctx.Redirect(http.StatusFound, "/home")

			// ctx.JSON(http.StatusOK, gin.H{
			// 	"token": token,
			// })
		} else {
			fmt.Println("redirection LOG>IN ")
			ctx.Redirect(http.StatusFound, "/logout")
			// ctx.JSON(http.StatusUnauthorized, nil)
		}
	})

	server.GET("/test", func(ctx *gin.Context) {
		ctx.JSON(200, gin.H{
			"message": "OK",
		})
	})

	server.NoRoute(handler.NoRoute)
	server.GET("/flex", handler.Flex)
	// server.NoRoute(func(c *gin.Context) {
	// c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
	// })

	// apiRoutes := server.Group("/api", middlewares.AuthorizeJWT())
	// {

	// }

	server.Run(":9090")
}
