package global

import (
	"database/sql"

	"fmt"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

// DB is a global variable to hold db connection
var DB *sql.DB

// ConnectDB opens a connection to the database
func InitDB() {
	e := godotenv.Load()
	if e != nil {
		panic(e.Error())
	}

	s_db_dialect := os.Getenv("DB_DIALECT")
	s_db_user := os.Getenv("DB_USER")
	s_db_password := os.Getenv("DB_PASSWORD")
	s_db_host := os.Getenv("DB_HOST")
	s_db_port := os.Getenv("DB_PORT")
	s_db_database := os.Getenv("DB_DATABASE")
	s_db_string := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", s_db_user, s_db_password, s_db_host, s_db_port, s_db_database)
	// db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/tvia2")
	db, err := sql.Open(s_db_dialect, s_db_string)
	db.SetMaxOpenConns(9000)
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxIdleConns(9000)
	if err != nil {
		panic(err.Error())
	}

	DB = db
}

// ConnectDB opens a connection to the database
func closeDB() {
	defer DB.Close()

}
