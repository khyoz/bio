package dto

type Credentials struct {
	Username string `form:"username"`
	Password string `form:"password"`
}

type RegistrationCredentials struct {
	// Firstname string `form:"firstname"`
	// Lastname  string `form:"lastname"`
	// Selas     string `form:"selas"`
	Email     string `form:"email"`
	Password  string `form:"password"`
	Password2 string `form:"password2"`
}
