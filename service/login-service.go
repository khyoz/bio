package service

import (
	"fmt"
	"gobio2/dto"
	"gobio2/global"
	"log"

	"golang.org/x/crypto/bcrypt"
)

type LoginService interface {
	Login(username string, password string) bool
}

type loginService struct {
	authorizedUsername string
	authorizedPassword string
}

func NewLoginService() LoginService {
	return &loginService{
		authorizedUsername: "pragmatic",
		authorizedPassword: "reviews",
	}
}

func (service *loginService) Login(username string, password string) bool {
	fmt.Println(username, password)
	return FindUserByEmail(username, password)
	// return service.authorizedUsername == username &&
	// 	service.authorizedPassword == password
}

func FindUserByEmail(username string, password string) bool {
	db := global.DB
	find := false
	rows, err := db.Query("select email,password from users where email = ?;", username)
	if err != nil {
		fmt.Print("error db", err.Error())
	}
	defer rows.Close()
	var dbcredentials dto.Credentials
	for rows.Next() {
		fmt.Println("db row")
		find = true
		err = rows.Scan(&dbcredentials.Username, &dbcredentials.Password)
	}

	//
	// the user is found in the database, we need to check the password now
	//
	// if find == true && true == false {
	// 	// check password
	// 	// pwdhash := hashAndSalt([]byte(dbcredentials.Password))
	// 	pwdhash := hashAndSalt([]byte(password))
	// 	fmt.Println("hashed pwd = ", []byte(pwdhash))
	// 	// hash, _ := bcrypt.GenerateFromPassword([]byte("noel"), bcrypt.MinCost)
	// 	// hash, _ := bcrypt.GenerateFromPassword(pwdhash, bcrypt.MinCost)
	// 	// fmt.Println("hash => ", hash, "end db --->", dbcredentials.Password)
	// 	// err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	// 	err := bcrypt.CompareHashAndPassword([]byte(pwdhash), []byte(password))
	// 	fmt.Println("error hash = ", err)
	// 	if err != nil {
	// 		log.Println(err)
	// 		return false
	// 	}
	// 	log.Println(err)
	// }

	if find == true {
		// check password
		// pwdhash := hashAndSalt([]byte(dbcredentials.Password))
		// fmt.Println("pwdhash = ", pwdhash)
		// match := CheckPasswordHash(dbcredentials.Password, hash)

		// pwdhash := hashAndSalt([]byte(password))
		// fmt.Println("hashed pwd = ", []byte(pwdhash))
		// fmt.Println("hashed passwd = ", []byte(dbcredentials.Password))
		// match := CheckPasswordHash(dbcredentials.Password, pwdhash)
		// err := bcrypt.CompareHashAndPassword([]byte(pwdhash), []byte(dbcredentials.Password))
		err := bcrypt.CompareHashAndPassword([]byte(dbcredentials.Password), []byte(password))
		// fmt.Println("match = ", match)
		// err := bcrypt.CompareHashAndPassword([]byte(dbcredentials.Password), []byte(pwdhash))
		// hash, _ := bcrypt.GenerateFromPassword([]byte("noel"), bcrypt.MinCost)
		// hash, _ := bcrypt.GenerateFromPassword(pwdhash, bcrypt.MinCost)
		// fmt.Println("hash => ", hash, "end db --->", dbcredentials.Password)
		// err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
		// err := bcrypt.CompareHashAndPassword([]byte(pwdhash), []byte(password))
		fmt.Println("error hash = ", err)
		if err != nil {
			log.Println(err)
			return false
		}
		log.Println(err)
	}

	fmt.Print("end db --->", dbcredentials.Password)
	rows.Close()
	return find
}

func hashAndSalt(pwd []byte) string {

	// Use GenerateFromPassword to hash & salt pwd
	// MinCost is just an integer constant provided by the bcrypt
	// package along with DefaultCost & MaxCost.
	// The cost can be any value you want provided it isn't lower
	// than the MinCost (4)
	hash, err := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	} // GenerateFromPassword returns a byte slice so we need to
	// convert the bytes to a string and return it
	return string(hash)
}
