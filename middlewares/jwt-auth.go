package middlewares

import (
	"fmt"
	"log"
	"net/http"

	"gobio2/service"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// AuthorizeJWT validates the token from the http request, returning a 401 if it's not valid
func AuthorizeJWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		const BearerSchema = "Bearer "
		authHeader := c.GetHeader("Authorization")
		tokenString := authHeader[len(BearerSchema):]

		token, err := service.NewJWTService().ValidateToken(tokenString)

		if token.Valid {
			claims := token.Claims.(jwt.MapClaims)
			log.Println("Claims[Name]: ", claims["name"])
			log.Println("Claims[Admin]: ", claims["admin"])
			log.Println("Claims[Issuer]: ", claims["iss"])
			log.Println("Claims[IssuedAt]: ", claims["iat"])
			log.Println("Claims[ExpiresAt]: ", claims["exp"])
		} else {
			log.Println(err)
			c.AbortWithStatus(http.StatusUnauthorized)
		}
	}
}

func AuthorizeCookie() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("check cookie")
		// c.AbortWithStatus(http.StatusUnauthorized)
		xtoken, err := c.Cookie("token")
		log.Println("token checked", xtoken)
		token, err := service.NewJWTService().ValidateToken(xtoken)
		log.Println("NOEL == ", err)

		if err != nil {
			c.Redirect(http.StatusTemporaryRedirect, "/login")
			c.AbortWithStatus(http.StatusTemporaryRedirect)
			c.Abort()
			return
		}
		if token.Valid {
			// if err != nil {
			claims := token.Claims.(jwt.MapClaims)
			log.Println("Token Claims[Name]: ", claims["name"])
			log.Println("Claims[Admin]: ", claims["admin"])
			log.Println("Claims[Issuer]: ", claims["iss"])
			log.Println("Claims[IssuedAt]: ", claims["iat"])
			log.Println("Claims[ExpiresAt]: ", claims["exp"])
		} else {
			log.Println(err)
			log.Println("not authorized noel")
			// ctx.Redirect(http.StatusTemporaryRedirect, "/login")
			// ctx.AbortWithStatus(http.StatusTemporaryRedirect)
			// return
			// ctx.Abort()
			// c.AbortWithStatus(http.StatusUnauthorized)
			// c.Redirect(http.StatusUnauthorized, "/login")
		}
	}
}
