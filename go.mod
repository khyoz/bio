module gobio2

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/howeyc/fsnotify v0.9.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/pilu/config v0.0.0-20131214182432-3eb99e6c0b9a // indirect
	github.com/pilu/fresh v0.0.0-20190826141211-0fa698148017 // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
)
